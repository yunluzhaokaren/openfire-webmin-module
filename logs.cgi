#!/usr/bin/perl
require 'openfire-lib.pl';
&ReadParse();

my @log_files = &openfire_logs_list_files();
my $response = '';

my $log_file = $in{'log_file'};
$log_file =~ s/^\s+|\s+$//;

if (! $log_file) {
    miniserv::http_error("400", "Bad request");
}


if ( ! grep { $_ eq $log_file } @log_files ) {
    miniserv::http_error("404", "The file '${log_file}' does not exist");
}

PrintHeader('UTF-8', 'text/plain');
print &openfire_logs_read_file($log_file);